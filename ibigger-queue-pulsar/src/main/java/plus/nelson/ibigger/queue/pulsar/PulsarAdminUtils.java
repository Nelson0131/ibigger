package plus.nelson.ibigger.queue.pulsar;

import org.apache.pulsar.client.admin.PulsarAdmin;
import org.apache.pulsar.client.admin.PulsarAdminException;
import org.apache.pulsar.common.policies.data.TenantInfo;
import plus.nelson.ibigger.configuration.iBiggerProperties;

import java.util.HashSet;
import java.util.List;

public class PulsarAdminUtils {
    public static void createTenantsOrNamespace(PulsarAdmin pulsarAdmin, iBiggerProperties properties, int ttlInSeconds) throws PulsarAdminException {
        if (!pulsarAdmin.tenants().getTenants().contains(properties.getTenant())) {
            List<String> clusters = pulsarAdmin.clusters().getClusters();
            pulsarAdmin.tenants().createTenant(properties.getTenant(), TenantInfo.builder().allowedClusters(new HashSet<>(clusters)).build());
        }
        String namespace = String.format("%s/%s", properties.getTenant(), properties.getNamespace());
        if (!pulsarAdmin.namespaces().getNamespaces(properties.getTenant()).contains(namespace)) {
            pulsarAdmin.namespaces().createNamespace(namespace);
        }

        pulsarAdmin.namespaces().setNamespaceMessageTTL(namespace, ttlInSeconds);
    }
}
