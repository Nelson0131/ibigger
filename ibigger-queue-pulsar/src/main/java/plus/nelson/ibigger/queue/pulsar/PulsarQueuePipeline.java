package plus.nelson.ibigger.queue.pulsar;

import org.apache.pulsar.client.admin.PulsarAdmin;
import org.apache.pulsar.client.api.*;
import plus.nelson.ibigger.configuration.iBiggerProperties;
import plus.nelson.ibigger.queue.*;
import plus.nelson.ibigger.utils.JsonUtils;
import plus.nelson.ibigger.utils.SerialUtils;
import plus.nelson.ibigger.utils.ThreadLockResourcePool;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;

@SuppressWarnings("ALL")
public class PulsarQueuePipeline implements QueuePipeline {
    public static final String BIGGER_DEFAULT = "iBiggerDefault";
    public static final String CONTEXT_NAME = "context";
    private iBiggerProperties properties;
    private iBiggerQueueManager queueManager;
    private PulsarClient client;
    private PulsarAdmin pulsarAdmin;

    private Map<String, Producer<byte[]>> producers = new ConcurrentHashMap<>();
    private Map<String, Producer<byte[]>> callbacks = new ConcurrentHashMap<>();
    private List<Consumer<byte[]>> consumers = new ArrayList<>();

    private final ThreadLockResourcePool<Message<byte[]>> lockResourcePool = new ThreadLockResourcePool<>();
    private final static ExecutorService callbackPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
    private final static ExecutorService listenerPool = Executors.newCachedThreadPool();


    private String basicTopic;
    private String tmpTopic;
    private static final int ttlInSeconds = 5 * 60;
    private static final Logger logger = Logger.getLogger("PulsarQueuePipeline");


    @Override
    public String getProtocol() {
        return "pulsar";
    }

    public PulsarClient getClient() {
        return client;
    }

    public iBiggerProperties getProperties() {
        return properties;
    }

    public iBiggerQueueManager getQueueManager() {
        return queueManager;
    }

    @Override
    public void initialize(iBiggerProperties properties, iBiggerQueueManager queueManager) {
        try {
            this.properties = properties;
            this.queueManager = queueManager;

            iBiggerServerFactory.initialize(properties.getThreadSize());

            pulsarAdmin = PulsarAdmin.builder()
                    .serviceHttpUrl(properties.getServiceUrl())
                    .build();

            client = PulsarClient.builder()
                    .serviceUrl(properties.getHost())
                    .ioThreads(properties.getThreadSize())
                    .build();

            basicTopic = String.format("%s://%s/%s/", properties.getTopicType(), properties.getTenant(), properties.getNamespace());
            tmpTopic = "non-" + basicTopic;

            PulsarAdminUtils.createTenantsOrNamespace(pulsarAdmin, properties, ttlInSeconds);

            String[] topics = queueManager.getListeners().keySet().stream().map(m -> basicTopic + m).toArray(String[]::new);

            // 基本处理监听
            if (topics.length > 0) {
                Consumer<byte[]> consumer = client.newConsumer().topic(topics)
                        .subscriptionName(BIGGER_DEFAULT)
                        .subscriptionType(SubscriptionType.Shared)
                        .subscribe();
                listenerPool.submit(() -> {
                    while (true) {
                        Message<byte[]> msg = consumer.receive();
                        try {
                            iBiggerServerFactory.submit(msg.getProperty(CONTEXT_NAME), msg.getValue(), queueManager, this);
                            consumer.acknowledge(msg);
                        } catch (Exception e) {
                            logger.severe(e.getMessage() + Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString));
                            consumer.negativeAcknowledge(msg);
                        }
                    }
                });
                consumers.add(consumer);
            }

            Consumer<byte[]> consumer = client.newConsumer()
                    .topic(tmpTopic + properties.getRandomServerKey())
                    .subscriptionName(BIGGER_DEFAULT)
                    .subscribe();
            listenerPool.submit(() -> {
                while (true) {
                    Message<byte[]> msg = consumer.receive();
                    try {
                        callbackPool.submit(() -> {
                            String property = msg.getProperty(CONTEXT_NAME);
                            QueueContext context = JsonUtils.convert(QueueContext.class, property);
                            if (context == null || context.getCallback() == null) {
                                return;
                            }
                            lockResourcePool.addAndUnlock(context.getCallback(), msg);
                        });
                        consumer.acknowledge(msg);
                    } catch (Exception e) {
                        logger.severe(e.getMessage() + Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString));
                        consumer.negativeAcknowledge(msg);
                    }
                }
            });
            consumers.add(consumer);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Object submit(QueueContext context, Object[] value) {
        try {
            Type backType = (Type) context.getBackType();
            String produceTopic = basicTopic + context.getInterfaceName();
            Producer<byte[]> producer = producers.get(produceTopic);
            if (producer == null) {
                synchronized (produceTopic) {
                    producer = producers.get(produceTopic);

                    if (producer == null) {
                        producer = client.newProducer()
                                .topic(produceTopic)
                                .create();
                        producers.put(produceTopic, producer);
                    }
                }
            }

            context.setCallbackServer(tmpTopic + properties.getRandomServerKey());
            context.setEffectiveTime(System.currentTimeMillis() + 10 * 1000);
            producer.newMessage()
                    .property(CONTEXT_NAME, JsonUtils.toJson(context))
                    .value(SerialUtils.generateParams(value).getBytes(StandardCharsets.UTF_8))
                    .sendAsync();

            Message<byte[]> receive = lockResourcePool.waitForObject(context.getCallback(), 10 * 1000l);

            if (receive == null) {
                throw new TimeoutException("Return Timeout!");
            }
            QueueContext backContext = JsonUtils.convert(QueueContext.class, receive.getProperty(CONTEXT_NAME));
            String type = backContext.getType();
            return SerialUtils.convertMethodReturn(QueueMessageType.valueOf(type), backType, receive.getValue());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void callback(QueueContext context, Object result) {
        try {
            Producer<byte[]> producer = callbacks.get(context.getCallbackServer());
            if (producer == null) {
                synchronized (callbacks) {
                    producer = callbacks.get(context.getCallbackServer());
                    if (producer == null) {
                        producer = client.newProducer()
                                .topic(context.getCallbackServer())
                                .create();
                        callbacks.put(context.getCallbackServer(), producer);
                    }
                }
            }

            producer.newMessage()
                    .property(CONTEXT_NAME, JsonUtils.toJson(context))
                    .value((byte[]) result)
                    .sendAsync();
        } catch (PulsarClientException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeProducer(Collection<Producer<byte[]>> closeables) {
        closeables.forEach(x -> {
            try {
                x.close();
            } catch (Exception e) {
            }
        });
    }

    private void closeConsumer(Collection<Consumer<byte[]>> closeables) {
        closeables.forEach(x -> {
            try {
                x.close();
            } catch (Exception e) {
            }
        });
    }

    @Override
    public void closeAll() {
        closeProducer(producers.values());
        closeProducer(callbacks.values());
        closeConsumer(consumers);

        try {
            client.close();
        } catch (PulsarClientException e) {
        }

    }

    @Override
    public CompletableFuture<Producer<?>> preCallback(String callbackServer) {
        return CompletableFuture.supplyAsync(() -> {
            synchronized (callbacks) {
                Producer<byte[]> producer = callbacks.get(callbackServer);
                if (producer == null) {
                    try {
                        producer = client.newProducer()
                                .topic(callbackServer)
                                .create();
                    } catch (PulsarClientException e) {
                    }
                    callbacks.put(callbackServer, producer);
                }
                return producer;
            }
        });
    }
}
