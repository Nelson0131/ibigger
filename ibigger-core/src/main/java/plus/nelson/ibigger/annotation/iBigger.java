package plus.nelson.ibigger.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.TYPE)
public @interface iBigger {
    String value() default "default";
}
