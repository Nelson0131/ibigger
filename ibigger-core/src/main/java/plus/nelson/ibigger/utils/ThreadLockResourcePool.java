package plus.nelson.ibigger.utils;

import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.*;

public class ThreadLockResourcePool<E> {
    private final Map<String, CountDownLatch> lockPool = new ConcurrentHashMap<>();
    private final Map<String, E> resourceCache = new ConcurrentHashMap<>();
    private final TreeMap<Long, LinkedList<String>> timeLimits = new TreeMap<>();

    private final ScheduledExecutorService scheduledService;
    private final ScheduledFuture<?> scheduledFuture;

    private final static long DEFAULT_TIMEOUT_LIMIT = 60 * 1000;
    private final static long DEFAULT_CLEAN_SCHEDULE = 60;

    private volatile boolean closePool = false;

    public ThreadLockResourcePool() {
        scheduledService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() * 4);
        scheduledFuture = scheduledService.scheduleAtFixedRate(() -> {
            try {
                while (timeLimits.firstKey() <= System.currentTimeMillis()) {
                    try {
                        LinkedList<String> keys;
                        synchronized (timeLimits) {
                            Map.Entry<Long, LinkedList<String>> entry = timeLimits.pollFirstEntry();
                            keys = entry.getValue();
                        }

                        for (String key : keys) {
                            resourceCache.remove(key);
                            lockPool.remove(key);
                        }
                    } catch (Exception ignored) {
                    }
                }
            } catch (Exception ignored) {
            }

        }, DEFAULT_CLEAN_SCHEDULE, DEFAULT_CLEAN_SCHEDULE, TimeUnit.SECONDS);
    }

    public ThreadLockResourcePool(int max) {
        scheduledService = Executors.newScheduledThreadPool(max);
        scheduledFuture = scheduledService.scheduleAtFixedRate(() -> {
            try {
                while (timeLimits.firstKey() <= System.currentTimeMillis()) {
                    try {
                        LinkedList<String> keys;
                        synchronized (timeLimits) {
                            Map.Entry<Long, LinkedList<String>> entry = timeLimits.pollFirstEntry();
                            keys = entry.getValue();
                        }

                        for (String key : keys) {
                            resourceCache.remove(key);
                            lockPool.remove(key);
                        }
                    } catch (Exception ignored) {
                    }
                }
            } catch (Exception ignored) {
            }

        }, DEFAULT_CLEAN_SCHEDULE, DEFAULT_CLEAN_SCHEDULE, TimeUnit.SECONDS);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public E waitForObject(String key, Long timeout) {
        if (closePool) {
            throw new RuntimeException("ResourcePool has been closed");
        }
        try {
            CountDownLatch lock = new CountDownLatch(1);
            lockPool.put(key, lock);
            long tempLimit = System.currentTimeMillis() + DEFAULT_TIMEOUT_LIMIT;
            synchronized (timeLimits) {
                LinkedList<String> keys = timeLimits.computeIfAbsent(tempLimit, k -> new LinkedList<>());
                keys.add(key);
            }
            lock.await(timeout, TimeUnit.MILLISECONDS);
        } catch (Exception ignored) {
        }
        lockPool.remove(key);
        return resourceCache.remove(key);
    }

    public void addAndUnlock(String key, E resource) {
        resourceCache.put(key, resource);
        try {
            lockPool.get(key).countDown();
        } catch (Exception ignored) {
        }
    }

    public void close() {
        closePool = true;
        scheduledService.schedule(() -> {
            timeLimits.clear();
            resourceCache.clear();
            lockPool.clear();
            scheduledFuture.cancel(false);
        }, 5, TimeUnit.SECONDS);

    }
}
