package plus.nelson.ibigger.utils;

import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class ResourcePool<E> {
    private final int max;
    private final ExecutorService taskPool;

    public ResourcePool(int max, Supplier<E> resourceConstructor) {
        if (max < 1) {
            max = 1;
        }
        int c = Runtime.getRuntime().availableProcessors() / 2;
        if (c == 0) {
            c = 1;
        }
        this.max = max;
        this.taskPool = new ThreadPoolExecutor(Math.min(c, max), max, 60 * 1000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), new ResourceThreadFactory<>(resourceConstructor));
    }


    public <T> T run(Function<E, T> executor) {
        Future<T> future = taskPool.submit(() -> {
            try {
                return executor.apply(ResourceThread.getResource());
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        try {
            return future.get();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getMax() {
        return max;
    }
}
