package plus.nelson.ibigger.utils;

import java.lang.reflect.Type;

public interface JsonConvertor {
    String toJson(Object o);

    <T> T convert(Type clz, String json);

    Object[] convert(Type[] classes, String[] jsons);
}
