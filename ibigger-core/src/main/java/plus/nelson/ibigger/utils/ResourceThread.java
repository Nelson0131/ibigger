package plus.nelson.ibigger.utils;

import java.util.function.Supplier;

public class ResourceThread<E> extends Thread {
    private final E resource;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T> T getResource() {
        if (Thread.currentThread() instanceof ResourceThread thread) {
            return (T) thread.resource;
        }
        return null;
    }

    public ResourceThread(ThreadGroup group, Runnable target, String name, long stackSize, Supplier<E> resourceConstructor) {
        super(group, target, name, stackSize);
        resource = resourceConstructor.get();
    }


}
