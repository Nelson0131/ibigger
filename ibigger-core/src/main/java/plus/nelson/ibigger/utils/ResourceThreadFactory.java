package plus.nelson.ibigger.utils;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class ResourceThreadFactory<E> implements ThreadFactory {
    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;

    private final Supplier<E> resourceConstructor;

    @SuppressWarnings("removal")
    public ResourceThreadFactory(Supplier<E> resourceConstructor) {
        this.resourceConstructor = resourceConstructor;
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        namePrefix = "pool-" + poolNumber.getAndIncrement() + "-thread-";
    }

    public ResourceThread<E> newThread(Runnable r) {
        ResourceThread<E> t = new ResourceThread<>(group, r, namePrefix + threadNumber.getAndIncrement(), 0, resourceConstructor);
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }
}
