package plus.nelson.ibigger.utils;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Random;

public class StringUtils {
    public static String substringAfterLast(String src, String sp) {
        if (src == null) {
            return null;
        }
        int i = src.lastIndexOf(sp);
        if (i < 0 || i == (src.length() - 1)) {
            return "";
        }
        return src.substring(i + 1);
    }

    public static String firstToLower(String src) {
        if (src == null || "".equals(src)) {
            return src;
        }
        String first = src.substring(0, 1).toLowerCase();
        return src.length() > 1 ? first + src.substring(1) : first;

    }

    public static String firstToUpper(String src) {
        if (src == null || "".equals(src)) {
            return src;
        }
        String first = src.substring(0, 1).toUpperCase();
        return src.length() > 1 ? first + src.substring(1) : first;

    }

    public static String getRandomWords(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int num = random.nextInt(26) + 65;
            builder.append((char) num);
        }
        return builder.toString();
    }

    public static String getMac() {
        try {
            NetworkInterface networkInterface = NetworkInterface.networkInterfaces()
                    .filter(f -> {
                        try {
                            return f.getHardwareAddress() != null;
                        } catch (SocketException e) {
                            return false;
                        }
                    })
                    .findFirst().orElse(null);
            if (networkInterface == null) {
                return null;
            }

            byte[] mac = networkInterface.getHardwareAddress();
            StringBuilder builder = new StringBuilder();
            for (byte b : mac) {
                builder.append(String.format("%02X-", b));
            }
            return builder.substring(0, builder.length() - 1);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean hasEmpty(String... words) {
        if (words == null || words.length == 0) {
            return true;
        }
        for (String str : words) {
            if (isEmpty(str)) {
                return true;
            }
        }
        return false;
    }

    public static String cutWord(String src, String from, String end) {
        int i = src.indexOf(from);
        int j = src.lastIndexOf(end);
        if (i < 0 || j < i) {
            return null;
        }
        if (i >= j) {
            return src;
        }
        return src.substring(i + from.length(), j);
    }
}
