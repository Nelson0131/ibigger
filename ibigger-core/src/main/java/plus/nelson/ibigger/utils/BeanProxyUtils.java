package plus.nelson.ibigger.utils;

import plus.nelson.ibigger.annotation.iBigger;
import plus.nelson.ibigger.handler.iBiggerClientHandler;
import plus.nelson.ibigger.queue.iBiggerQueueFactory;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public class BeanProxyUtils {
    private static final Map<String, Object> CLIENTS = new HashMap<>();

    public static <T> T createProxy(Class<T> clz) {
        iBigger annotation = clz.getAnnotation(iBigger.class);
        if (annotation == null || !clz.isInterface()) {
            return null;
        }
        T bean = (T) Proxy.newProxyInstance(BeanProxyUtils.class.getClassLoader(), new Class[]{clz}, new iBiggerClientHandler(annotation.value(), clz));
        iBiggerQueueFactory.addBean(annotation.value(), clz.getName(), bean);
        return bean;
    }

    public static <T> T getClientBean(Class<T> clz) {
        if (clz.isInterface()) {
            return (T) CLIENTS.computeIfAbsent(clz.getName(), (k)-> createProxy(clz));
        }
        return null;
    }
}
