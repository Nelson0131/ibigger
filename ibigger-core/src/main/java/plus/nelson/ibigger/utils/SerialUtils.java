package plus.nelson.ibigger.utils;

import plus.nelson.ibigger.queue.QueueMessageType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * utils for serial
 *
 * @author nelson
 */
public class SerialUtils {
    private static final Map<String, Long> SERIAL_CACHE = new HashMap<>();
    private static final Map<String, Class<?>> CLZ_CACHE = new ConcurrentHashMap<>();
    public static final int MAX_LIMIT_SERIAL = 16;

    public static long getNext(String key) {
        synchronized (SERIAL_CACHE) {
            long serial = SERIAL_CACHE.computeIfAbsent(key, k -> 0L);
            if (serial > Math.pow(serial, MAX_LIMIT_SERIAL)) {
                serial = 0;
            }
            SERIAL_CACHE.put(key, serial + 1);
            return serial;
        }
    }

    public static String generateCallBackKey(String interfaceName, String method) {
        String key = interfaceName + "." + method;
        return String.format("%s_%s_%s", key, System.getProperty("MAC_ADDRESS"), getNext(key));
    }

    public static String generateMethodKey(String methodName, List<String> paramTypeNames) {
        return paramTypeNames.stream().collect(Collectors.joining("#", methodName + "-", ""));
    }

    public static String generateMethodKey(String methodName, Class<?>[] paramTypes) {
        return Arrays.stream(paramTypes).map(Class::getName).collect(Collectors.joining("#", methodName + "-", ""));
    }

    public static String generateParams(Object... objects) {
        StringBuilder builder = new StringBuilder();
        builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<xml>");
        for (Object o : objects) {
            if (o == null) {
                builder.append("<param></param>");
            } else {
                builder.append("<param><![CDATA[").append(JsonUtils.toJson(o)).append("]]></param>");
            }
        }
        builder.append("</xml>");
        return builder.toString();
    }

    public static String[] parseParams(String xmlParams) {
        if (StringUtils.isEmpty(xmlParams)) {
            return null;
        }
        int from = xmlParams.indexOf("<param>");
        int last = xmlParams.lastIndexOf("</param>");

        if (from < 0 || from >= last) {
            return null;
        }

        xmlParams = xmlParams.substring(from, last);

        return Arrays.stream(xmlParams.split("</param>"))
                .filter(StringUtils::isNotEmpty)
                .map(x -> StringUtils.cutWord(x, "<![CDATA[", "]]>"))
                .toArray(String[]::new);
    }

    public static Map<String, Class<?>[]> convertMethodKey(String methodKey) {
        String[] words = methodKey.split("-");
        HashMap<String, Class<?>[]> result = new HashMap<>(1);
        if (words.length > 1) {
            result.put(words[0], Arrays.stream(words[1].split("#")).map(m -> {
                try {
                    Class<?> clz = CLZ_CACHE.get(m);
                    if (clz == null) {
                        clz = Class.forName(m);
                        CLZ_CACHE.put(m, clz);
                    }
                    return clz;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                return null;
            }).toArray(Class<?>[]::new));
        } else {
            result.put(words[0], null);
        }

        return result;
    }

    public static Object[] convertMethodParams(QueueMessageType type, Type[] types, byte[] value) {
        Object[] result = null;
        if (type == QueueMessageType.Json) {
            String[] params = parseParams(new String(value));
            if (params == null) {
                return null;
            }
            result = JsonUtils.convert(types, params);
        } else if (type == QueueMessageType.Error) {
            result = new Object[]{JsonUtils.convert(Exception.class, new String(value))};
        }

        return result;
    }

    public static Object convertMethodReturn(QueueMessageType messageType, Type returnType, byte[] value) throws Exception {
        Object result = null;
        if (messageType == QueueMessageType.Json) {
            result = JsonUtils.convert(returnType, new String(value));
        } else if (messageType == QueueMessageType.Error) {
            throw parseException(value);
        }

        return result;
    }

    public static Exception parseException(byte[] bytes) {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes); ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream)) {
            return (Exception) ois.readObject();
        } catch (Exception e) {
            throw new RuntimeException("error with parseException");
        }
    }

    public static byte[] convertException(Exception e) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); ObjectOutputStream stream = new ObjectOutputStream(outputStream)) {
            stream.writeObject(e);
            return outputStream.toByteArray();
        } catch (Exception exception) {
            return new byte[0];
        }
    }

}
