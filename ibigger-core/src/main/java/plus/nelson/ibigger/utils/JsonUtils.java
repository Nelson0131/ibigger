package plus.nelson.ibigger.utils;

import java.lang.reflect.Type;

public class JsonUtils {
    private static JsonConvertor convertor;

    public static void register(JsonConvertor convertor) {
        JsonUtils.convertor = convertor;
    }

    public static <T> T convert(Type clz, String json) {
        return convertor.convert(clz, json);
    }

    public static Object[] convert(Type[] classes, String[] jsons) {
        return convertor.convert(classes, jsons);
    }

    public static String toJson(Object obj) {
        return convertor.toJson(obj);
    }

    public static void register(String className) {
        try {
            Class<?> clz = Class.forName(className);
            register((JsonConvertor) clz.getDeclaredConstructor().newInstance());
        } catch (Exception ignored) {

        }
    }
}
