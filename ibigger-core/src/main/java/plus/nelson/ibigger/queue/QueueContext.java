package plus.nelson.ibigger.queue;

import lombok.Data;
import plus.nelson.ibigger.utils.StringUtils;

import java.io.Serial;
import java.io.Serializable;

@Data
public class QueueContext implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String type;
    private String interfaceName;
    private String method;
    private String callbackServer;
    private String callback;
    private Object backType;
    private Long effectiveTime;

    public QueueContext() {

    }

    public boolean hasEmpty() {
        return StringUtils.hasEmpty(type,interfaceName,method,callback);
    }
}
