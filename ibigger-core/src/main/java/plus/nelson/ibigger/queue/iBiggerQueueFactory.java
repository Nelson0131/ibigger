package plus.nelson.ibigger.queue;

import plus.nelson.ibigger.configuration.iBiggerProperties;
import plus.nelson.ibigger.utils.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("AlibabaCollectionInitShouldAssignCapacity")
public class iBiggerQueueFactory {
    private static final Map<String, QueuePipeline> PIPELINES = new ConcurrentHashMap<>();
    private static final List<iBiggerProperties> PROPERTIES = new LinkedList<>();
    private static final Map<String, iBiggerQueueManager> MANAGERS = new ConcurrentHashMap<>();
    private static final Map<String, Map<String, Object>> INTERFACE_CACHE = new ConcurrentHashMap<>();

    public static void addQueuePipeline(QueuePipeline pipeline) {
        PIPELINES.put(pipeline.getProtocol(), pipeline);
    }

    public static synchronized void addBiggerProperties(iBiggerProperties property) {
        PROPERTIES.add(property);
    }

    public static synchronized void addBean(String profile, String interfaceName, Object bean) {
        Map<String, Object> profileMap = INTERFACE_CACHE.get(profile);
        if (profileMap == null) {
            profileMap = new ConcurrentHashMap<>();
            INTERFACE_CACHE.put(profile, profileMap);
        }
        profileMap.putIfAbsent(interfaceName, bean);
    }

    public static iBiggerQueueManager getDefaultManager() {
        return MANAGERS.get("default");
    }

    public static iBiggerQueueManager getManager(String profile) {
        return MANAGERS.get(profile);
    }

    public static void initialize() {
        CompletableFuture<Void> macAddress = CompletableFuture.runAsync(() -> System.getProperties().put("MAC_ADDRESS", StringUtils.getMac()));

        PROPERTIES.forEach(x -> {
            String host = x.getHost();
            String profile = x.getProfile();

            int i = host.indexOf("://");
            if (i < 0) {
                return;
            }
            String protocol = host.substring(0, i);

            QueuePipeline queuePipeline = PIPELINES.get(protocol);
            Map<String, Object> infCache = INTERFACE_CACHE.get(profile);

            if (queuePipeline == null || infCache == null) {
                return;
            }

            iBiggerQueueManager manager = new iBiggerQueueManager(x, queuePipeline);
            infCache.forEach((k, v) -> manager.addServerBean(v, k));
            manager.initialize();
            MANAGERS.put(profile, manager);
        });
        try {
            macAddress.get();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public static void close() {
        for (Map.Entry<String, iBiggerQueueManager> entry : MANAGERS.entrySet()) {
            entry.getValue().close();
        }
    }
}
