package plus.nelson.ibigger.queue;

import plus.nelson.ibigger.handler.iBiggerServerHandler;
import plus.nelson.ibigger.utils.StringUtils;

import java.util.concurrent.*;

public class iBiggerServerFactory {
    public static ExecutorService serverPool;

    public synchronized static void initialize(int maxSize) {
        if (serverPool != null) {
            int processors = Runtime.getRuntime().availableProcessors() * 4;
            serverPool = new ThreadPoolExecutor(processors, Math.max(maxSize, processors), 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        }
    }

    public static void submit(String contextStr, byte[] value, iBiggerQueueManager queueManager, QueuePipeline queuePipeline) {
        if (StringUtils.isEmpty(contextStr)) {
            throw new RuntimeException("QueueContext is null or empty");
        }
        serverPool.submit(new iBiggerServerHandler(contextStr, value, queueManager, queuePipeline));
    }
}
