package plus.nelson.ibigger.queue;

import plus.nelson.ibigger.configuration.iBiggerProperties;

import java.util.concurrent.CompletableFuture;

public interface QueuePipeline {
    String getProtocol();

    void initialize(iBiggerProperties properties, iBiggerQueueManager manager);

    Object submit(QueueContext context,Object[] value);

    void callback(QueueContext context, Object result);

    void closeAll();

    default CompletableFuture<?> preCallback(String callbackServer) {
        return null;
    }
}
