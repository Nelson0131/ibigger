package plus.nelson.ibigger.queue;

public enum QueueMessageType {
    // Json Type
    Json,
    // Xml Type
    Xml,
    // Bytes Type
    Bytes,
    // Error
    Error,
}
