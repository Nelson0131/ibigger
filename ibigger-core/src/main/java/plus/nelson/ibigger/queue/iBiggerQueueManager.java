package plus.nelson.ibigger.queue;

import plus.nelson.ibigger.configuration.iBiggerProperties;
import plus.nelson.ibigger.handler.iBiggerClientHandler;
import plus.nelson.ibigger.utils.SerialUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class iBiggerQueueManager {
    private final iBiggerProperties properties;
    private final Map<String, List<String>> listeners = new ConcurrentHashMap<>();
    private final Map<String, Object> servers = new ConcurrentHashMap<>();
    private final QueuePipeline queuePipeline;
    public final static HashSet<String> EXCLUDE_METHODS = new HashSet<>();

    static {
        EXCLUDE_METHODS.add("wait-long#int");
        EXCLUDE_METHODS.add("wait-");
        EXCLUDE_METHODS.add("wait-long");
        EXCLUDE_METHODS.add("equals-java.lang.Object");
        EXCLUDE_METHODS.add("toString-");

        EXCLUDE_METHODS.add("hashCode-");
        EXCLUDE_METHODS.add("getClass-");
        EXCLUDE_METHODS.add("notify-");
        EXCLUDE_METHODS.add("notifyAll-");
    }

    public iBiggerQueueManager(iBiggerProperties properties, QueuePipeline queuePipeline) {
        this.properties = properties;
        this.queuePipeline = queuePipeline;
    }

    public void addServerBean(Object bean, String interfaceName) {
        servers.put(interfaceName, bean);

        if (iBiggerClientHandler.isCurrentHandler(bean)) {
            return;
        }

        Method[] methods = bean.getClass().getMethods();
        LinkedList<String> methodList = Arrays.stream(methods)
                .map(x -> SerialUtils.generateMethodKey(x.getName(), x.getParameterTypes()))
                .filter(f -> !EXCLUDE_METHODS.contains(f))
                .collect(Collectors.toCollection(LinkedList::new));
        listeners.putIfAbsent(interfaceName, methodList);
    }

    public Object submit(String interfaceName, Method method, Object... params) {
        if (queuePipeline == null) {
            throw new RuntimeException("QueuePipeline is null!");
        }

        QueueContext queueContext = new QueueContext();
        queueContext.setType(QueueMessageType.Json.name());
        queueContext.setMethod(SerialUtils.generateMethodKey(method.getName(), method.getParameterTypes()));
        queueContext.setInterfaceName(interfaceName);
        queueContext.setCallback(SerialUtils.generateCallBackKey(queueContext.getInterfaceName(), queueContext.getMethod()));
        queueContext.setBackType(method.getGenericReturnType());
        return queuePipeline.submit(queueContext, params);
    }

    public QueuePipeline getQueuePipeline() {
        return queuePipeline;
    }

    public Map<String, List<String>> getListeners() {
        return listeners;
    }

    public Object getService(String interfaceName) {
        return servers.get(interfaceName);
    }

    public iBiggerProperties getProperties() {
        return properties;
    }

    public void initialize() {
        queuePipeline.initialize(properties, this);
    }

    public void close() {
        queuePipeline.closeAll();
    }
}
