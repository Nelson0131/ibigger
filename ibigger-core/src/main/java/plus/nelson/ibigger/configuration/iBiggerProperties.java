package plus.nelson.ibigger.configuration;

import lombok.Data;
import plus.nelson.ibigger.utils.StringUtils;

@Data
public class iBiggerProperties {
    private String profile = "default";
    private String host;
    private String serviceUrl = "http://localhost:8080";
    private String topicType = "persistent";
    private String tenant = "iBiggerDefault";
    private String namespace = "iBiggerSystemCore";
    private int threadSize = Runtime.getRuntime().availableProcessors() * 8;

    private String randomServerKey = StringUtils.getMac() + "_" + StringUtils.getRandomWords(8);
}
