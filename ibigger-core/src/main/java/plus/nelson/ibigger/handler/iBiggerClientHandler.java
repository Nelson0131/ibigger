package plus.nelson.ibigger.handler;

import lombok.Data;
import plus.nelson.ibigger.queue.iBiggerQueueFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@SuppressWarnings("ALL")
@Data
public class iBiggerClientHandler implements InvocationHandler {

    private String interfaceName;

    private String profile;

    private Class<?> interfaceClz;

    public iBiggerClientHandler(String profile, Class<?> interfaceClz) {
        this.interfaceName = interfaceClz.getName();
        this.interfaceClz = interfaceClz;
        this.profile = profile;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return iBiggerQueueFactory.getManager(profile).submit(interfaceName, method, args);
    }

    public static boolean isCurrentHandler(Object o) {
        return o instanceof Proxy proxy && Proxy.getInvocationHandler(proxy) instanceof iBiggerClientHandler;
    }
}
