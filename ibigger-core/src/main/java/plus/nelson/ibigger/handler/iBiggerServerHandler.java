package plus.nelson.ibigger.handler;

import plus.nelson.ibigger.queue.QueueContext;
import plus.nelson.ibigger.queue.QueueMessageType;
import plus.nelson.ibigger.queue.QueuePipeline;
import plus.nelson.ibigger.queue.iBiggerQueueManager;
import plus.nelson.ibigger.utils.JsonUtils;
import plus.nelson.ibigger.utils.SerialUtils;

import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public record iBiggerServerHandler(String contextStr, byte[] value,
                                   iBiggerQueueManager queueManager,
                                   QueuePipeline queuePipeline) implements Runnable {
    private final static Logger logger = Logger.getLogger("iBiggerServerHandler");

    @Override
    public void run() {
        QueueContext context = JsonUtils.convert(QueueContext.class, contextStr);
        QueueContext returnContext = new QueueContext();

        returnContext.setCallback(context.getCallback());
        returnContext.setCallbackServer(context.getCallbackServer());

        try {
            List<String> methods = queueManager.getListeners().get(context.getInterfaceName());

            if (!methods.contains(context.getMethod())) {
                throw new RuntimeException("cannot find method for " + context.getMethod());
            }

            QueueMessageType type = QueueMessageType.valueOf(context.getType());

            if (QueueMessageType.Json != type) {
                throw new RuntimeException(type + " will be provided in the future");
            }

            Object service = queueManager.getService(context.getInterfaceName());
            if (service == null) {
                throw new RuntimeException(String.format("cannot find [%s]'s handler", context.getInterfaceName()));
            }

            Class<?> clz = service.getClass();
            Map<String, Class<?>[]> methodCache = SerialUtils.convertMethodKey(context.getMethod());
            Map.Entry<String, Class<?>[]> next = methodCache.entrySet().iterator().next();
            Method method = clz.getMethod(next.getKey(), next.getValue());
            Object[] params = SerialUtils.convertMethodParams(type, method.getGenericParameterTypes(), value);
            Object result = method.invoke(service, params);

            returnContext.setType(type.name());
            queuePipeline.callback(returnContext, JsonUtils.toJson(result).getBytes(StandardCharsets.UTF_8));
        } catch (Exception throwable) {
            logger.severe("executing with an error " + throwable);
            returnContext.setType(QueueMessageType.Error.name());
            queuePipeline.callback(returnContext, SerialUtils.convertException(throwable));
        }
    }
}
