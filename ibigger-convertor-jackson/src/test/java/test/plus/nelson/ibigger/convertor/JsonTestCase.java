package test.plus.nelson.ibigger.convertor;

import org.junit.jupiter.api.Test;
import plus.nelson.ibigger.convertor.JacksonConvertor;
import plus.nelson.ibigger.utils.JsonUtils;
import plus.nelson.ibigger.utils.SerialUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class JsonTestCase {
    @Test
    public void testToJson() {
        JsonUtils.register(new JacksonConvertor(16));
        ExecutorService executorService = Executors.newFixedThreadPool(32);
        List<Future<String>> last = new ArrayList<>(14285790);
        for (var i = 0; i < 9999999; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("test1", i);
            map.put("test2", "hahaha");
            map.put("aaaa", Map.of("bbbb", "cccc"));
            Future<String> submit = executorService.submit(() -> JsonUtils.toJson(map));
            last.add(submit);
        }

        last.forEach(x-> {
            try {
                x.get();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        });

    }

    @Test
    public void testToBean() {
        Map<String, Object> map = new HashMap<>();
        map.put("test1", 1);
        map.put("test2", "hahaha");
        map.put("aaaa", Map.of("bbbb", "cccc"));

        JsonUtils.register(new JacksonConvertor());
        String s = JsonUtils.toJson(map);
        TestModel convert = JsonUtils.convert(TestModel.class, s);
        System.out.println(s);
    }

    @Test
    public void testSerialParams() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("testM", "hahaha");
        TestModel testModel = new TestModel();
        testModel.setTest1(1);
        testModel.setTest2("2");
        testModel.setAaaa(map);
        ArrayList<TestModel> models = new ArrayList<>();
        models.add(testModel);
        models.add(testModel);


        JsonUtils.register(new JacksonConvertor());
        String s1 = SerialUtils.generateParams(testModel, models);

        Type[] testParamsFuncs = this.getClass().getMethod("testParamsFunc", TestModel.class, List.class).getGenericParameterTypes();

        String[] params = SerialUtils.parseParams(s1);
        Object[] convert = JsonUtils.convert(testParamsFuncs, params);
        System.out.println(s1);
    }

    public void testParamsFunc(TestModel testModel, List<TestModel> ls) {

    }
}

class TestModel {
    private String test2;
    private int test1;
    private Map<String, String> aaaa;

    public String getTest2() {
        return test2;
    }

    public void setTest2(String test2) {
        this.test2 = test2;
    }

    public int getTest1() {
        return test1;
    }

    public void setTest1(int test1) {
        this.test1 = test1;
    }

    public Map<String, String> getAaaa() {
        return aaaa;
    }

    public void setAaaa(Map<String, String> aaaa) {
        this.aaaa = aaaa;
    }
}
