package plus.nelson.ibigger.convertor;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.SimpleType;
import plus.nelson.ibigger.utils.JsonConvertor;
import plus.nelson.ibigger.utils.ResourcePool;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.logging.Logger;

public class JacksonConvertor implements JsonConvertor {
    private final ResourcePool<ObjectMapper> pool;
    private final static Logger logger = Logger.getLogger("JacksonConvertor");

    public JacksonConvertor() {
        pool = new ResourcePool<>(Runtime.getRuntime().availableProcessors() * 4, ObjectMapper::new);
        logger.info("JacksonConvertor has been started");
    }

    public JacksonConvertor(int max) {
        pool = new ResourcePool<>(max, ObjectMapper::new);
        logger.info("JacksonConvertor has been started");
    }

    @Override
    public String toJson(Object o) {
        if (o == null) {
            return null;
        }
        return pool.run(mapper -> {
            try {
                return mapper.writeValueAsString(o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public <T> T convert(Type clz, String json) {
        return pool.run(mapper -> {
            try {
                return mapper.readValue(json, convertType(mapper, clz));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public Object[] convert(Type[] classes, String[] jsons) {
        if (classes.length != jsons.length) {
            throw new RuntimeException("参数传递有误！");
        }

        Object[] result = new Object[classes.length];

        for (int i = 0; i < classes.length; i++) {
            Type aClass = classes[i];
            String json = jsons[i];
            result[i] = json == null ? null : convert(aClass, json);
        }
        return result;
    }

    public JavaType convertType(ObjectMapper mapper, Type type) {
        if (type instanceof ParameterizedType parameterizedType) {
            Type rawType = parameterizedType.getRawType();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            JavaType[] javaTypes = new JavaType[actualTypeArguments.length];
            for (int i = 0; i < actualTypeArguments.length; i++) {
                javaTypes[i] = convertType(mapper, actualTypeArguments[i]);
            }
            return mapper.getTypeFactory().constructParametricType((Class<?>) rawType, javaTypes);
        }

        return SimpleType.constructUnsafe((Class<?>) type);
    }

}
