package plus.nelson.ibigger.starter.configuration;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import plus.nelson.ibigger.configuration.iBiggerProperties;

@ConfigurationProperties(prefix = "ibigger.configuration")
@Component
@ConditionalOnProperty(value = "ibigger.configuration.multiple", havingValue = "false", matchIfMissing = true)
@Data
public class iBiggerConfigurationProperties {

    private String profile = "default";
    private String host = "pulsar://localhost:6650";
    private String serviceUrl = "http://localhost:8080";
    private String topicType = "persistent";
    private String tenant = "iBiggerDefault";
    private String namespace = "iBiggerSystemCore";
    private int timeout = 10000;

    private int threadSize = Runtime.getRuntime().availableProcessors() * 8;

    private boolean multiple = false;


    public iBiggerProperties toProperties() {
        iBiggerProperties properties = new iBiggerProperties();
        BeanUtils.copyProperties(this, properties);
        return properties;
    }
}
