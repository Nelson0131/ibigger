package plus.nelson.ibigger.starter;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import plus.nelson.ibigger.annotation.iBigger;
import plus.nelson.ibigger.queue.QueuePipeline;
import plus.nelson.ibigger.utils.JsonConvertor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class IBiggerDefinitionScanner extends ClassPathBeanDefinitionScanner {
    protected static final String DEFAULT_RESOURCE_PATTERN = "**/*.class";
    protected static final HashMap<String, BeanDefinition> CLIENTS = new HashMap<>();
    protected static final HashSet<String> SERVICES = new HashSet<>();
    private ResourcePatternResolver resourcePatternResolver;

    public IBiggerDefinitionScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters, Environment environment, ResourceLoader resourceLoader) {
        super(registry, useDefaultFilters, environment, resourceLoader);
    }

    @Override
    protected void registerDefaultFilters() {
        addIncludeFilter(new AnnotationTypeFilter(iBigger.class));
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Set<BeanDefinition> findCandidateComponents(String basePackage) {
        Set<BeanDefinition> candidates = new LinkedHashSet<>();
        try {
            String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    resolveBasePackage(basePackage) + '/' + DEFAULT_RESOURCE_PATTERN;
            Resource[] resources = getResourcePatternResolver().getResources(packageSearchPath);
            boolean traceEnabled = logger.isTraceEnabled();
            for (Resource resource : resources) {
                logger.info("Scanning " + resource);
                try {
                    MetadataReader metadataReader = getMetadataReaderFactory().getMetadataReader(resource);
                    ScannedGenericBeanDefinition sbd = new ScannedGenericBeanDefinition(metadataReader);
                    sbd.setSource(resource);

                    AnnotationMetadata metadata = sbd.getMetadata();
                    List<String> interfaceNames = List.of(metadata.getInterfaceNames());
                    if (interfaceNames.contains(JsonConvertor.class.getName())) {
                        candidates.add(sbd);
                        continue;
                    }

                    MergedAnnotation<iBigger> mergedAnnotation = metadata.getAnnotations().get(iBigger.class);

                    if (mergedAnnotation.isPresent()) {
                        if (metadata.isInterface() && !SERVICES.contains(metadata.getClassName())) {
                            CLIENTS.put(metadata.getClassName(), sbd);
                            candidates.add(sbd);
                        } else if (metadata.isConcrete()) {
                            CLIENTS.keySet().stream()
                                    .filter(interfaceNames::contains)
                                    .findAny()
                                    .ifPresent(key -> candidates.remove(CLIENTS.get(key)));
                            candidates.add(sbd);
                        }
                    } else if (Arrays.stream(metadata.getInterfaceNames()).anyMatch(f -> QueuePipeline.class.getName().equals(f))) {
                        candidates.add(sbd);
                    }

                } catch (FileNotFoundException ex) {
                    if (traceEnabled) {
                        logger.trace("Ignored non-readable " + resource + ": " + ex.getMessage());
                    }
                } catch (Throwable ex) {
                    throw new BeanDefinitionStoreException(
                            "Failed to read candidate component class: " + resource, ex);
                }
            }
        } catch (IOException ex) {
            throw new BeanDefinitionStoreException("I/O failure during classpath scanning", ex);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }

        return candidates;
    }

    private ResourcePatternResolver getResourcePatternResolver() {
        if (this.resourcePatternResolver == null) {
            this.resourcePatternResolver = new PathMatchingResourcePatternResolver();
        }
        return this.resourcePatternResolver;
    }

}
