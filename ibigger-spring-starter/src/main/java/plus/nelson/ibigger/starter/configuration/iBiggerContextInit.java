package plus.nelson.ibigger.starter.configuration;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import plus.nelson.ibigger.queue.iBiggerQueueFactory;

@SuppressWarnings("NullableProblems")
public class iBiggerContextInit implements ApplicationListener<ApplicationStartedEvent> {
    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        iBiggerQueueFactory.initialize();
    }
}
