package plus.nelson.ibigger.starter.annotation;

import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;
import plus.nelson.ibigger.starter.IBiggerDefinitionRegistrar;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({IBiggerDefinitionRegistrar.class})
@EnableAutoConfiguration
@AutoConfigurationPackage(basePackages = "plus.nelson")
public @interface iBiggerScan {
    @AliasFor("basePackages") String[] value() default {};

    @AliasFor("value") String[] basePackages() default {};

    Class<?>[] basePackageClasses() default {};
}
