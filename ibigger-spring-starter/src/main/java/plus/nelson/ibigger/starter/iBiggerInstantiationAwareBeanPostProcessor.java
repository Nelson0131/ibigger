package plus.nelson.ibigger.starter;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;
import plus.nelson.ibigger.annotation.iBigger;
import plus.nelson.ibigger.queue.QueuePipeline;
import plus.nelson.ibigger.queue.iBiggerQueueFactory;
import plus.nelson.ibigger.starter.configuration.iBiggerConfigurationMultiProperties;
import plus.nelson.ibigger.starter.configuration.iBiggerConfigurationProperties;
import plus.nelson.ibigger.utils.BeanProxyUtils;
import plus.nelson.ibigger.utils.JsonConvertor;
import plus.nelson.ibigger.utils.JsonUtils;

import java.util.Arrays;

@SuppressWarnings("NullableProblems")
@Component
public class iBiggerInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    @SuppressWarnings("NullableProblems")
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if (beanClass.isInterface() && beanClass.getAnnotation(iBigger.class) != null) {
            return BeanProxyUtils.getClientBean(beanClass);
        }

        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        iBigger annotation = bean.getClass().getAnnotation(iBigger.class);

        if (bean instanceof iBiggerConfigurationProperties configurationProperties) {
            iBiggerQueueFactory.addBiggerProperties(configurationProperties.toProperties());
        } else if (bean instanceof QueuePipeline pipeline) {
            iBiggerQueueFactory.addQueuePipeline(pipeline);
        } else if (annotation != null) {
            Arrays.stream(bean.getClass().getInterfaces())
                    .filter(f -> f.getAnnotation(iBigger.class) != null)
                    .findAny()
                    .ifPresent(clz -> iBiggerQueueFactory.addBean(annotation.value(), clz.getName(), bean));
        } else if (bean instanceof JsonConvertor convertor) {
            JsonUtils.register(convertor);
        } else if (bean instanceof iBiggerConfigurationMultiProperties properties) {
            properties.getProperties().forEach((k,v)->{
                v.setProfile(k);
                iBiggerQueueFactory.addBiggerProperties(v.toProperties());
            });
        }
        return bean;
    }
}
