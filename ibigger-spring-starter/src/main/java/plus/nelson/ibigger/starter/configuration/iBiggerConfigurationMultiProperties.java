package plus.nelson.ibigger.starter.configuration;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@ConfigurationProperties(prefix = "ibigger.configuration.multi")
@Component
@ConditionalOnProperty(value = "ibigger.configuration.multiple", havingValue = "true")

@Data
public class iBiggerConfigurationMultiProperties {
    private Map<String, iBiggerConfigurationProperties> properties;
}
