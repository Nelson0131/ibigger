package plus.nelson.ibigger.starter.configuration;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import plus.nelson.ibigger.queue.iBiggerQueueFactory;

@SuppressWarnings("NullableProblems")
public class iBiggerContextShutdown implements ApplicationListener<ContextClosedEvent> {
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        iBiggerQueueFactory.close();
    }
}
