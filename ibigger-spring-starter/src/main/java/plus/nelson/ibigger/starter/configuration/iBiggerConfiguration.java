package plus.nelson.ibigger.starter.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "plus.nelson.ibigger.starter")
public class iBiggerConfiguration {

}
